// import React, { Fragment } from "react";
import React from "react";
import PropTypes from "prop-types";

//FC
const PrimeraApp = ({ saludo, subtitulo }) => {
  // const saludo = {
  //   nombre: "Fernando",
  //   edad: 34,
  // };

  // const saludo = "Hola mundo";

  // console.log(props);

  return (
    <>
      {/* <pre>{JSON.stringify(saludo, null, 3)}</pre> */}
      <h1> {saludo} </h1>
      <p> {subtitulo}</p>
    </>
  );
};

PrimeraApp.propTypes = {
  saludo: PropTypes.string.isRequired,
};
PrimeraApp.defaultProps = {
  subtitulo: "Soy un subtitulo",
};

export default PrimeraApp;
