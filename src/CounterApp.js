import React from "react";
import PropTypes from "prop-types";

const CounterApp = ({ numero }) => {
  //handleUp
  const handleAdd = (e) => {
    console.log(e);
  };

  return (
    <>
      <h1>CounterApp</h1>
      <h2> {numero}</h2>

      <button onClick={handleAdd}> +1 </button>
    </>
  );
};

CounterApp.propTypes = {
  numero: PropTypes.number,
};

export default CounterApp;
